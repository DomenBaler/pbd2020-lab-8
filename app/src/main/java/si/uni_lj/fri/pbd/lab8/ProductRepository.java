package si.uni_lj.fri.pbd.lab8;

import android.app.Application;
import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Database;

public class ProductRepository {


    private MutableLiveData<List<Products>> searchResults =
            new MutableLiveData<>();
    private LiveData<List<Products>> allProducts;

    // TODO: Add DAO reference
    private ProductDao productDao;

    // TODO: Add a constructor
    public ProductRepository(Application application){
        ProductRoomDatabase db;
        db = ProductRoomDatabase.getDatabase(application);
        productDao = db.productDao();
        allProducts = productDao.getAllProducts();
    }

    public void insertProduct(final Products newproduct) {
        // TODO: run query to insert a product on the executor
        ProductRoomDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                productDao.insertProduct(newproduct);
            }
        });

    }


    public void deleteProduct(final String name) {
        // TODO: run query to delete a product on the executor
        ProductRoomDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                productDao.deleteProduct(name);
            }
        });
    }

    public void findProduct(final String name) {
        // TODO: run query on the executor, postValue to searchResults
        ProductRoomDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                searchResults.postValue(productDao.findProduct(name));
            }
        });

    }

    public LiveData<List<Products>> getAllProducts() {
        return allProducts;
    }

    public MutableLiveData<List<Products>> getSearchResults() {
        return searchResults;
    }

}
