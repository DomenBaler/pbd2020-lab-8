package si.uni_lj.fri.pbd.lab8;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "products")
public class Products {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "productId")
    private int id;
    @ColumnInfo(name = "productName")
    private String name;
    private int quantity;

    public void setId(int id){
        this.id = id;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setQuantity(int quantity){
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getQuantity() {
        return quantity;
    }
}
