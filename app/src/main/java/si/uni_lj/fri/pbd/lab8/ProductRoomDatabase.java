package si.uni_lj.fri.pbd.lab8;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Products.class}, version = 1,exportSchema = false)
public abstract class ProductRoomDatabase extends RoomDatabase {

    private static ProductRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    static ProductRoomDatabase getDatabase(final Context context){
        if(INSTANCE == null){
            synchronized (ProductRoomDatabase.class){
                if(INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), ProductRoomDatabase.class, "product_database").build();
                }
            }
        }
        return INSTANCE;
    }

    public abstract ProductDao productDao();
}
