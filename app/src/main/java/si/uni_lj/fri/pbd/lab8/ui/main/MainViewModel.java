package si.uni_lj.fri.pbd.lab8.ui.main;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import si.uni_lj.fri.pbd.lab8.ProductRepository;
import si.uni_lj.fri.pbd.lab8.Products;

public class MainViewModel extends AndroidViewModel {

    // TODO: add LiveData fields (allProducts and searchResults)
    private LiveData<List<Products>> allProducts;
    private MutableLiveData<List<Products>> searchResults;

    // TODO: add ProductRepository field
    private ProductRepository repository;

    public MainViewModel (Application application) {
        super(application);

        // TODO: set repository, allProducts, and searchResults
        repository = new ProductRepository(application);
        allProducts = repository.getAllProducts();
        searchResults = repository.getSearchResults();

    }

    // TODO: add getSearchResults and getAllProducts functions
    MutableLiveData<List<Products>> getSearchResults(){
        return searchResults;
    }

    LiveData<List<Products>> getAllProducts(){
        return allProducts;
    }

    // TODO: add functions for inserting, deleting, and finding a product
    public void insertProduct(Products product){
        repository.insertProduct(product);
    }

    public void findProduct(String name){
        repository.findProduct(name);
    }

    public void deleteProduct(String name){
        repository.deleteProduct(name);
    }

}
